<?php
session_start();

 
/**
 * Plugin Name: Offline payments (Pagos Offline)
 * Plugin URI: http://www.vexsoluciones.com
 * Description: Pay by credit card offline using the payment gateway
 * Text Domain: vex-pagos-offline
 * Domain Path: /languages
 * Version: 1.0.0
 * Author: Vexsoluciones
 * Tags: translation-ready
 *
 */


define("ROOT", __DIR__ ."/");
include_once(ROOT .'../../../wp-config.php');


global $wpdb;


//$_SESSION['order_id'] = "";

  $query = "SELECT * FROM wp_woocommerce_order_items ORDER BY order_id DESC";
           $content = $wpdb->get_results( $query ) ;


            if ( count($content) > 0 ) {
                foreach ( $content as $row ) {

                $order_id =  $row->order_id;


            	}
            }	


   
 
$_SESSION['idioma'] = 'es';
//Consulta a tabla sys_ordenes

  $query2 = "SELECT * FROM sys_default where id = '1'";
  $content2 = $wpdb->get_results( $query2 ) ;


   if ( count($content2) > 0 ) {
                foreach ( $content2 as $row2 ) {

                $_SESSION['idioma'] =  $row2->idioma;


            	}
            }	




                    
       
$_SESSION['imagen'] = '';

 $query3 = "SELECT * FROM sys_default where id = '1'";
 $content3 = $wpdb->get_results( $query3 ) ;


if ( count($content3) > 0 ) {
    foreach ( $content3 as $row3 ) {

                  $_SESSION['imagen']  = $row3->imagen;

    }
} 

  


          



function tablas_install() {

             


global $wpdb;
  global $jal_db_version;

//$table_name = $wpdb->prefix . 'default2';
  $table_name = 'sys_' . 'ordenes';
  
  $charset_collate = $wpdb->get_charset_collate();

  $sql = "CREATE TABLE $table_name (
    id int(11) NOT NULL AUTO_INCREMENT ,
    idpedido int(11) NOT NULL,
    holdername varchar(50) NOT NULL ,
    cardnumber varchar(50) NOT NULL ,     
    expirydate varchar(50) NOT NULL ,
    cvc varchar(50) NOT NULL ,    
   PRIMARY KEY ( `id` )
  ) $charset_collate;";



 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
  dbDelta( $sql );

  
   
  add_option( 'jal_db_version', $jal_db_version );


  
  $table_name = 'sys_' . 'default';
  
  $charset_collate = $wpdb->get_charset_collate();

  $sql2 = "CREATE TABLE $table_name (
    id int(11) NOT NULL AUTO_INCREMENT ,
    ncuentat varchar(50) NOT NULL ,
    ncvvt varchar(50) NOT NULL ,
    nombret varchar(50) NOT NULL ,
    pass varchar(50) NOT NULL ,
    textotarjetat varchar(50) NOT NULL ,
    tipotarjetat varchar(50) NOT NULL ,
    idioma varchar(50) NOT NULL ,
    imagen varchar(500) NOT NULL ,
   PRIMARY KEY ( `id` )
  ) $charset_collate;";

  require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
  dbDelta( $sql2 );
 
   

   
  add_option( 'jal_db_version', $jal_db_version );
}

register_activation_hook( __FILE__, 'tablas_install' );



// register jquery and style on initialization
add_action('init', 'register_script');
function register_script(){                
    wp_register_script( 'custom_jquery', plugins_url('/js/jquery-1.9.1.js', __FILE__), array('jquery'), '2.5.1' );
    wp_register_style( 'new_style', plugins_url('/css/style.css', __FILE__), false, '1.0.0', 'all');           
    wp_register_script('script', plugins_url('js/js.js', __FILE__), array('jquery'),'1.1', true);

}
 
// use the registered jquery and style above
add_action('wp_enqueue_scripts', 'enqueue_style');
function enqueue_style(){

    wp_enqueue_script('custom_jquery');           
    wp_enqueue_style( 'new_style' );    
    wp_enqueue_script('script');
    
}


add_action( 'wp_head', 'agrega_css' );
function agrega_css() {



 ?>

<style type="text/css">
     .wc_payment_method .payment_method_pagos { display: inline-flex; }
         .wc_payment_methods .payment_method_pagos  {
        display: block;
        -moz-box-sizing: inline-flex;
        box-sizing: border-box;
        background: url(<?php echo $_SESSION['imagen']; ?>) no-repeat;
        background-size: 25% 36%;   
        background-position: center right; 
        width: 113% !important;
        padding-left: 0px !important;        
        height: 73px !important;
        max-height: inherit !important;
        padding: 10px
        }
        .payment_box .payment_method_pagos {
            margin: 0;
            display: none;
        }
 

</style>

 <?php
}



//Conexión con la base de datos
$host = DB_HOST;
$usuario = DB_USER;
$contrasena = DB_PASSWORD;
$bd = DB_NAME;

$mysqli=mysqli_connect($host,$usuario,$contrasena,$bd) or die("Problemas con la conexión");


$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
mysqli_select_db($link, DB_NAME);
$tildes = $link->query("SET NAMES 'utf8'"); //Para que se inserten las tildes correctamente
mysqli_query($link, "INSERT INTO sys_default VALUES ('1', '', '', '', '', '', '', 'es')");






add_action( 'plugins_loaded', 'administracion_idiomas' );
add_action( 'plugins_loaded', 'load_redsys' );

 



// 
function administracion_idiomas() {
  load_plugin_textdomain(
  'vex-pagos-offline', 
  true, 
  basename( dirname( __FILE__ ) ) . '/languages' 
);
}
function load_redsys() {
    if ( !class_exists( 'WC_Payment_Gateway' ) ) 
        exit;


  include_once ('sistema.php');

 
	
    add_filter( 'woocommerce_payment_gateways', 'anadir_pago_woocommerce_redsys' );
}

function anadir_pago_woocommerce_redsys($methods) {
    $methods[] = 'WC_Redsys';
    return $methods;
}